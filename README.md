## Olá, meu nome é Ayrton Costa.
Sou formado em Tec. em Sistemas da Computação pela UFF.


#### Minhas habilidades:

- Docker 
- Kubernetes 
- Prometheus.io 
- Grafana 
- Linux | OpenSuse 
- Python 

Meu principal [projeto](https://gitlab.com/ayrton2/SigaInfra) demonstrando essas habilidades.